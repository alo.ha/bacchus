﻿using Bacchus.Models;
using Bacchus.ViewModels;
using System.Collections.Generic;
using System.Threading.Tasks;
using System.Web.Mvc;

namespace Bacchus.Controllers
{
    public class AuctionViewModelController : Controller
    {
        private IAuctionRepository repository = new AuctionRepository();

        public AuctionViewModelController(IAuctionRepository repo)
        {
            repository = repo;
        }
        // GET: AuctionViewModel
        public async Task<ActionResult> Index()
        {
            var model = new AuctionViewModel
            {
                Categories = await repository.GetCategoriesAsync(),
                Products = await repository.GetProductAsync()
            };
            return View(model);

        }
        // GET: AuctionViewModel filter
        public async Task<JsonResult> FilterCategory(string category)
        {
            if (category == null)
            {
                category = "";
            }
            var Products = await repository.GetProductByCategoryAsync(category);
            
            return Json(Products);
        }
        //GET: OffersViewModel
        public async Task<ActionResult> Offers()
        {            
            var model = repository.GetOffersAsync();
            return View(model);

        }
        public ActionResult Bid(string productId)
        {
            if (productId == null)
            {
                return HttpNotFound();
            }

            BidViewModel offer = new BidViewModel();
            offer.productId = productId;

            return View(offer);
        }
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Bid([Bind(Include = "FirstName,LastName,BiddingSum, productId")] BidViewModel offers)
        {
            if (ModelState.IsValid)
            {
                repository.Bid(offers);
                return RedirectToAction("Index");
            }

            return View(offers);
        }
    }
}