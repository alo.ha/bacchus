﻿using System.ComponentModel.DataAnnotations;

namespace Bacchus.ViewModels
{
    public class BidViewModel
    {
        [Display(Name = "First Name")]
        [MinLength(3)]
        public string FirstName { get; set; }
        [Display(Name = "Last Name")]
        [MinLength(3)]
        public string LastName { get; set; }
        [Display(Name = "Bid")]
        public float BiddingSum { get; set; }
        public string productId { get; set; }
    }
}