﻿using Bacchus.Models;
using System.Collections.Generic;
using System.Web.Mvc;

namespace Bacchus.ViewModels
{
    public class AuctionViewModel
    {
        public IEnumerable<Product> Products { get; set; }
        public IEnumerable<SelectListItem> Categories { get; set; }
    }
}