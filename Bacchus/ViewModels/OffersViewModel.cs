﻿using System.ComponentModel.DataAnnotations;

namespace Bacchus.ViewModels
{
    public class OffersViewModel
    {
        [Display(Name = "Offer ID")]
        public int OfferID { get; set; }
        [Display(Name ="First name")]
        public string FirstName { get; set; }
        [Display(Name ="Last name")]
        public string LastName { get; set; }
        [Display(Name ="Username")]
        public string UserName { get; set; }
        [Display(Name ="Bidded sum")]
        public float BiddingSum { get; set; }
        [Display(Name ="Product ID")]
        public string productId { get; set; }
    }
}