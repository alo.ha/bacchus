﻿using System.ComponentModel.DataAnnotations;

namespace Bacchus.Models
{
    public class Product
    {
        public string productId { get; set; }
        [Display(Name ="Product Name")]
        public string productName { get; set; }
        [Display(Name ="Product Description")]
        public string productDescription { get; set; }
        [Display(Name ="Product Category")]
        public string productCategory { get; set; }
        [Display(Name ="End date")]
        
        public string biddingEndDate { get; set; }
    }

}