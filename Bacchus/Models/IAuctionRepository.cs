﻿using Bacchus.ViewModels;
using System.Collections.Generic;
using System.Threading.Tasks;
using System.Web.Mvc;

namespace Bacchus.Models
{
    public interface IAuctionRepository
    {
        Task<IEnumerable<Product>> GetProductAsync();
        Task<IEnumerable<Product>> GetProductByCategoryAsync(string category);
        Task<IEnumerable<SelectListItem>> GetCategoriesAsync();
        IEnumerable<OffersViewModel> GetOffersAsync();
        void Bid(BidViewModel bidVm);
    }
}
