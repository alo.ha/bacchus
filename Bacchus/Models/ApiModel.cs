﻿using System;

namespace Bacchus.Models
{
    public class ApiModel
    {
        public string productId { get; set; }
        public string productName { get; set; }
        public string productDescription { get; set; }
        public string productCategory { get; set; }
        public DateTime biddingEndDate { get; set; }
    }
}