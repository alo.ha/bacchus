﻿using Bacchus.ViewModels;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;

namespace Bacchus.Models
{
    public class AuctionRepository : IAuctionRepository
    {
        private BacchusContext context = new BacchusContext();

        private static string baseUrl = "http://uptime-auction-api.azurewebsites.net/api/Auction";

        private string FormateDateTime(DateTime dt)
        {
            StringBuilder sb = new StringBuilder();
            sb.AppendFormat("{0:00}",dt.Day);
            sb.Append('-');
            sb.AppendFormat("{0:00}", dt.Month);
            sb.Append('-');
            sb.AppendFormat("{0:00}", dt.Year);
            sb.Append(' ');
            sb.AppendFormat("{0:00}", dt.Hour);
            sb.Append(':');
            sb.AppendFormat("{0:00}", dt.Minute);
            sb.Append(':');
            sb.AppendFormat("{0:00}", dt.Second);

            return sb.ToString();
        }
        private static async Task<HttpResponseMessage> GetHttpResponseMessage(string url)
        {
            using (var httpClient = new HttpClient())
            {
                HttpResponseMessage response = await httpClient.GetAsync(url);
                response.EnsureSuccessStatusCode();
                return response;
            }
        }
        public void Bid(BidViewModel bidVm)
        {
            var datetimeArray = DateTime.Now.ToString().Split(' ');
            StringBuilder sb = new StringBuilder();
            foreach (var item in datetimeArray)
            {
                sb.Append(item);
            }
            Offers offers = new Offers
            {
                UserName = bidVm.FirstName + bidVm.LastName + sb.ToString(),
                FirstName = bidVm.FirstName,
                LastName = bidVm.LastName,
                BiddingSum = bidVm.BiddingSum,
                productId = bidVm.productId
            };
            context.Offers.Add(offers);
            context.SaveChanges();
        }
        public async Task<IEnumerable<Product>> GetProductAsync()
        {
            var response = await GetHttpResponseMessage(baseUrl);
            var productJson = await response.Content.ReadAsStringAsync();
            var apiModel = JsonConvert.DeserializeObject<List<ApiModel>>(productJson);
            List<Product> product = new List<Product>();
            foreach (var item in apiModel)
            {
                product.Add(new Product
                {
                    productName = item.productName,
                    productCategory = item.productCategory,
                    productDescription = item.productDescription,
                    productId = item.productId,
                    biddingEndDate = FormateDateTime(item.biddingEndDate)
                });
            }
            return product;
        }

        public async Task<IEnumerable<SelectListItem>> GetCategoriesAsync()
        {
            var product = await GetProductAsync();
            //var categories = product.Select(x => new SelectListItem
            //{
            //    Value = "",
            //    Text = x.productCategory
            //}).Distinct();
            var categories = product.Select(x => x.productCategory).Distinct();
            var selectList = categories.Select(x => new SelectListItem
            {
                Value = "",
                Text = x
            }).ToList();
            selectList.Add(new SelectListItem
            {
                Value = "",
                Text = "All"
            });
            selectList.Reverse();
            return selectList;
        }

        public async Task<IEnumerable<Product>> GetProductByCategoryAsync(string category)
        {
            var product = await GetProductAsync();
            if (!category.Equals("All"))
            {
                product = product.Where(x => x.productCategory == category)
                   .Select(x => new Product
                   {
                       productCategory = x.productCategory,
                       productId = x.productId,
                       productDescription = x.productDescription,
                       productName = x.productName,
                       biddingEndDate = x.biddingEndDate
                   }).ToList();
            }

            return product;

        }

        public IEnumerable<OffersViewModel> GetOffersAsync()
        {
            var offers = context.Offers;
            List<OffersViewModel> offersVM = new List<OffersViewModel>();
            foreach (var item in offers)
            {
                offersVM.Add(new OffersViewModel
                {
                    OfferID = item.OfferID,
                    FirstName = item.FirstName,
                    LastName = item.LastName,
                    BiddingSum = item.BiddingSum,
                    productId = item.productId,
                    UserName = item.UserName
                });
            }
            return offersVM;
        }
    }
}