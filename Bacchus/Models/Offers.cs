﻿using System.ComponentModel.DataAnnotations;

namespace Bacchus.Models
{
    public class Offers
    {
        [Key]
        public int OfferID { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string UserName { get; set; }
        public float BiddingSum { get; set; }        
        public string productId { get; set; }
    }
}