namespace Bacchus.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class fixedOfferstableproductId : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("dbo.Offers", "product_productId", "dbo.Products");
            DropIndex("dbo.Offers", new[] { "product_productId" });
            AddColumn("dbo.Offers", "productId", c => c.String());
            DropColumn("dbo.Offers", "product_productId");
            DropTable("dbo.Products");
        }
        
        public override void Down()
        {
            AddColumn("dbo.Offers", "product_productId", c => c.String(maxLength: 128));
            DropColumn("dbo.Offers", "productId");
            CreateIndex("dbo.Offers", "product_productId");
            AddForeignKey("dbo.Offers", "product_productId", "dbo.Products", "productId");
        }
    }
}
