namespace Bacchus.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddingOffersandProducttables : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Offers",
                c => new
                    {
                        OfferID = c.Int(nullable: false, identity: true),
                        FirstName = c.String(),
                        LastName = c.String(),
                        UserName = c.String(),
                        BiddingSum = c.Single(nullable: false),
                        product_productId = c.String(maxLength: 128),
                    })
                .PrimaryKey(t => t.OfferID)
                .ForeignKey("dbo.Products", t => t.product_productId)
                .Index(t => t.product_productId);
            
            CreateTable(
                "dbo.Products",
                c => new
                    {
                        productId = c.String(nullable: false, maxLength: 128),
                        OfferID = c.Int(nullable: false),
                        productName = c.String(),
                        productDescription = c.String(),
                        productCategory = c.String(),
                        biddingEndDate = c.DateTime(nullable: false),
                    })
                .PrimaryKey(t => t.productId);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Offers", "product_productId", "dbo.Products");
            DropIndex("dbo.Offers", new[] { "product_productId" });
            DropTable("dbo.Products");
            DropTable("dbo.Offers");
        }
    }
}
