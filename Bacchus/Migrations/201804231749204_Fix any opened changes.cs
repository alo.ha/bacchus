namespace Bacchus.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Fixanyopenedchanges : DbMigration
    {
        public override void Up()
        {
            AlterColumn("dbo.Offers", "FirstName", c => c.String());
            AlterColumn("dbo.Offers", "LastName", c => c.String());
        }
        
        public override void Down()
        {
            AlterColumn("dbo.Offers", "LastName", c => c.String(nullable: false));
            AlterColumn("dbo.Offers", "FirstName", c => c.String(nullable: false));
        }
    }
}
