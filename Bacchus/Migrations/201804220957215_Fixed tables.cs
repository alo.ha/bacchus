namespace Bacchus.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Fixedtables : DbMigration
    {
        public override void Up()
        {
            AlterColumn("dbo.Offers", "FirstName", c => c.String(nullable: false));
            AlterColumn("dbo.Offers", "LastName", c => c.String(nullable: false));
        }
        
        public override void Down()
        {
            AlterColumn("dbo.Offers", "LastName", c => c.String());
            AlterColumn("dbo.Offers", "FirstName", c => c.String());
        }
    }
}
